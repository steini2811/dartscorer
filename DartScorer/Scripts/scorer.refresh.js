﻿var requestURL = "";
var initiated = false;

setRequestURL = function (url) {
    requestURL = url;
    refreshScoring();
    setInterval(refreshScoring, 2000);
}

refreshScoring = function () {
    if (requestURL != "") {
        $.ajax({
            type: 'GET',
            url: requestURL,
            dataType: 'json',
            success: function (result) {
                if (!initiated) {
                    $("#match_1 .pkte").html(result.Player1.Score.CurrentPoints);
                    $("#match_1 .name").html(result.Player1.Player.PlayerName);

                    $("#match_2 .pkte").html(result.Player2.Score.CurrentPoints);
                    $("#match_2 .name").html(result.Player2.Player.PlayerName);

                    if (result.Settings.BestOfSets == 1) {
                        $(".sets").css("display", "none");
                        $("#descr .name").html("Best of " + result.Settings.BestOfLegs);
                    }
                    else {
                        $("#descr .name").html("Best of " + result.Settings.BestOfSets);
                    }

                    initiated = true;
                } else {
                    countPointsDown(1, result.Player1.Score.CurrentPoints);
                    countPointsDown(2, result.Player2.Score.CurrentPoints);
                }

                if (result.Player1Active) {
                    $("#descr .pkte").removeClass("right");
                    $("#descr .pkte").addClass("left");
                } else {
                    $("#descr .pkte").removeClass("left");
                    $("#descr .pkte").addClass("right");
                }

                $("#match_1 .sets").html(result.Player1.Score.CurrentSets);
                $("#match_1 .legs").html(result.Player1.Score.CurrentLegs);
                $("#match_1 .avg").html(result.Player1.Statistic.ThreeDartAverage);
                $("#match_1 .his").html(result.Player1.Statistic.HighScore);
                $("#match_1 .hif").html(result.Player1.Statistic.HighFinish);
                $("#match_1 .a60").html(result.Player1.Statistic.ScoresOf60);
                $("#match_1 .a100").html(result.Player1.Statistic.ScoresOf100);
                $("#match_1 .a140").html(result.Player1.Statistic.ScoresOf140 + result.Player1.Statistic.ScoresOf180);

                var points1 = result.Settings.StartingPoints;
                var table1String = "<tr><th></th><td>" + points1 + "</td></tr>";

                for (var i = 0; i < result.Player1.Score.CurrentLegScores.length; i++) {
                    var score = result.Player1.Score.CurrentLegScores[i];
                    points1 -= score;
                    table1String += "<tr><th>" + score + "</th><td>" + points1 + "</td></tr>"
                }

                $("#match_2 .sets").html(result.Player2.Score.CurrentSets);
                $("#match_2 .legs").html(result.Player2.Score.CurrentLegs);
                $("#match_2 .avg").html(result.Player2.Statistic.ThreeDartAverage);
                $("#match_2 .his").html(result.Player2.Statistic.HighScore);
                $("#match_2 .hif").html(result.Player2.Statistic.HighFinish);
                $("#match_2 .a60").html(result.Player2.Statistic.ScoresOf60);
                $("#match_2 .a100").html(result.Player2.Statistic.ScoresOf100);
                $("#match_2 .a140").html(result.Player2.Statistic.ScoresOf140 + result.Player2.Statistic.ScoresOf180);

                var points2 = result.Settings.StartingPoints;
                var table2String = "<tr><th></th><td>" + points2 + "</td></tr>";

                for (var j = 0; j < result.Player2.Score.CurrentLegScores.length; j++) {
                    var score = result.Player2.Score.CurrentLegScores[j];
                    points2 -= score;
                    table2String += "<tr><th>" + score + "</th><td>" + points2 + "</td></tr>"
                }

                $("#tab1").html(table1String);
                $("#tab2").html(table2String);
            }
        });
    }
};

countPointsDown = function (playerNumber, newScore) {
    var currentScore = $("#match_" + (playerNumber) + " .pkte").html();
    var score = currentScore - newScore;

    if (score > 0) {
        if (score == 180) {
            s180();
        }

        var i = 0;
        var a = setInterval(function () {
            var temp = $("#match_" + (playerNumber) + " .pkte").html();
            temp--;
            $("#match_" + (playerNumber) + " .pkte").html(temp);
            i++;
            if (i == score) {
                clearInterval(a);
            }
        }, (1500 / score));
    }
    else {
        $("#match_" + (playerNumber) + " .pkte").html(newScore);
    }
};

s180 = function () {
    $(".pyro").fadeIn(500);
    $("#section").fadeOut(500);
    $("#div180").delay(500).fadeIn(300);
    $("#div180").delay(300).fadeOut(300);
    $("#div180").delay(300).fadeIn(300);
    $("#div180").delay(300).fadeOut(300);
    $("#div180").delay(300).fadeIn(300);
    $("#div180").delay(1500).fadeOut(300);
    setTimeout(function () {
        $("#section").fadeIn(500);
        $(".pyro").fadeOut(500);
    }, 5500);
};
