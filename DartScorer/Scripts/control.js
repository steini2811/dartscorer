﻿var requestURL = "";
var postURL = "";
var undoURL = "";
var redoURL = "";
var saveURL = "";

setRequestURL = function (url) {
    requestURL = url;
    refreshScoring();
    setInterval(refreshScoring, 1000);
}

setPostURL = function (url) {
    postURL = url;
}

setUndoURL = function (url) {
    undoURL = url;
}

setRedoURL = function (url) {
    redoURL = url;
}

setSaveURL = function (url) {
    saveURL = url;
    setInterval(function () {
        $.ajax({
            type: "GET",
            url: saveURL,
            cache: false
        });
    }, 60000);
}

refreshScoring = function () {
    if (requestURL != "") {
        $.ajax({
            type: 'GET',
            url: requestURL,
            dataType: 'json',
            success: function (result) {
                $("#points .pl1").html(result.Player1.Score.CurrentPoints);
                $("#names .pl1").html(result.Player1.Player.PlayerName);

                $("#points .pl2").html(result.Player2.Score.CurrentPoints);
                $("#names .pl2").html(result.Player2.Player.PlayerName);

                if (result.Settings.BestOfSets == 1) {
                    $(".sets").css("display", "none");
                }

                if (result.Player1Active) {
                    $("#points .dec").removeClass("right");
                    $("#points .dec").addClass("left");
                } else {
                    $("#points .dec").removeClass("left");
                    $("#points .dec").addClass("right");
                }

                $("#sets .pl1").html(result.Player1.Score.CurrentSets);
                $("#legs .pl1").html(result.Player1.Score.CurrentLegs);

                $("#sets .pl2").html(result.Player2.Score.CurrentSets);
                $("#legs .pl2").html(result.Player2.Score.CurrentLegs);

                $('#overview input#undo').prop("disabled", !result.UndoPossible);
                $('#overview input#redo').prop("disabled", !result.RedoPossible);
            }
        });
    }
};

$(document).ready(function () {
    var dart = 1;
    var timeout;

    function setActiveThrow() {
        $('#overview input[type="text"]').removeClass('chosen');
        $('#overview #' + dart).addClass('chosen');
    };

    function clearThrows() {
        $('#overview input[type="text"]').attr('value', '');
    };

    function reset() {
        dart = 1;
        clearThrows();
        setActiveThrow();
    }

    function ajax_req() {
        $.ajax({
            type: "GET",
            url: postURL + "/" + $('#overview input#1').attr('value') + "/" + $('#overview input#2').attr('value') + "/" + $('#overview input#3').attr('value'),
            cache: false
        });
    }

    reset();

    $('input').click(function () {
        clearTimeout(timeout);
    });

    $('#overview input[type="text"]').click(function () {
        dart = $(this).attr('id');
        setActiveThrow();
    });

    $('#control_buttons input').click(function () {
        $('#overview #' + dart).attr('value', $(this).attr('value'));
        dart++;
        setActiveThrow();

        if (dart == 4) {
            timeout = setTimeout(function () {
                ajax_req();
                reset();
            }, 500);
        }
    });

    $('#overview input#confirm').click(function () {
        ajax_req();
        reset();
    });

    $('#overview input#reset').click(function () {
        reset();
    });

    $('#overview input#undo').click(function () {
        $.ajax({
            type: "GET",
            url: undoURL,
            cache: false
        });
        reset();
    });

    $('#overview input#redo').click(function () {
        $.ajax({
            type: "GET",
            url: redoURL,
            cache: false
        });
        reset();
    });

});