﻿using DartScorer.Models.Storage;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DartScorer
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // load data from files
            PlayerStorage.Instance.ImportStorageItems();
            MatchStorage.Instance.ImportStorageItems();
            MatchStorage.Instance.AssignPlayersToMatches();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
