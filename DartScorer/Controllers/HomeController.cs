﻿using DartScorer.Models;
using DartScorer.Models.Storage;
using System;
using System.Web.Mvc;

namespace DartScorer.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            SelectList listItems = new SelectList(MatchStorage.Instance.StorageItems.Values, "ID", "DisplayName");

            return View(listItems);
        }

        [HttpPost]
        public ActionResult Index(int matchID, string gameAction)
        {
            if (gameAction == "scorer")
            {
                return RedirectToAction("Scorer", "Home", new { id = matchID });
            }

            if (gameAction == "control")
            {
                return RedirectToAction("Control", "Home", new { id = matchID });
            }

            throw new ArgumentException("Unexpected value for argument gameAction.");
        }

        [HttpGet]
        public ActionResult NewMatch()
        {
            SelectList listItems = new SelectList(PlayerStorage.Instance.StorageItems.Values, "ID", "PlayerName");

            return View(listItems);
        }

        [HttpPost]
        public ActionResult NewMatch(int player1, int player2, int startPoints, int sets, int legs)
        {
            Player player1Object = PlayerStorage.Instance.StorageItems[player1];
            Player player2Object = PlayerStorage.Instance.StorageItems[player2];

            DartMatch match = new DartMatch(player1Object, player2Object, new MatchSetting(startPoints, sets, legs, true));

            return RedirectToAction("Scorer", "Home", new { id = match.ID });
        }

        [HttpGet]
        public ActionResult NewPlayer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewPlayer(string name)
        {
            Player newPlayer = new Player(name);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Control(int id)
        {
            ViewBag.MatchID = id;

            return View();
        }

        public ActionResult Scorer(int id)
        {
            ViewBag.MatchID = id;

            return View();
        }
    }
}