﻿using DartScorer.Models;
using DartScorer.Models.Storage;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace DartScorer.Controllers
{
    public class APIController : Controller
    {
        public string Match(int id)
        {
            DartMatch match = MatchStorage.Instance.StorageItems[id];
            string json = JsonConvert.SerializeObject(match);

            return json;
        }

        public void Score(int id, string dart1, string dart2, string dart3)
        {
            ThrowGroup group = new ThrowGroup(DartValues.Parse(dart1), DartValues.Parse(dart2), DartValues.Parse(dart3));
            Score(id, group);
        }

        [NonAction]
        private void Score(int id, ThrowGroup throwGroup)
        {
            DartMatch match = MatchStorage.Instance.StorageItems[id];
            match.Score(throwGroup);
        }

        public void SaveToFile()
        {
            PlayerStorage.Instance.ExportStorageItems();
            MatchStorage.Instance.ExportStorageItems();
        }

        public void Undo(int id)
        {
            DartMatch match = MatchStorage.Instance.StorageItems[id];
            match.Undo();
        }

        public void Redo(int id)
        {
            DartMatch match = MatchStorage.Instance.StorageItems[id];
            match.Redo();
        }
    }
}