﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;

namespace DartScorer.Models
{
    public class PlayerStatistic
    {
        public uint ThrownPoints { get; set; } = 0;
        public uint ThrownDarts { get; set; } = 0;
        public List<byte> HighScores { get; set; } = new List<byte>() { 0 };
        public List<byte> HighFinishes { get; set; } = new List<byte>() { 0 };
        public uint ScoresOf60 { get; set; } = 0;
        public ushort ScoresOf100 { get; set; } = 0;
        public ushort ScoresOf140 { get; set; } = 0;
        public ushort ScoresOf180 { get; set; } = 0;

        [XmlIgnore]
        public int HighScore => HighScores[0];

        [XmlIgnore]
        public int HighFinish => HighFinishes[0];

        public string ThreeDartAverage
        {
            get
            {
                if (ThrownDarts > 0)
                {
                    double avg = (float)ThrownPoints / ThrownDarts * 3;
                    return avg.ToString("F2", new CultureInfo("de-DE"));
                }
                return 0.ToString("F2", new CultureInfo("de-DE"));
            }
        }

        public PlayerStatistic() { }

        public void RegisterScore(ThrowGroup group)
        {
            ThrownDarts += group.DartCount;

            if (group.Result != ThrowResult.BUSTED)
            {
                byte score = group.PointSum;
                ThrownPoints += score;

                AddToHighList(HighScores, score);

                if (score == 180)
                {
                    ScoresOf180++;
                }
                else if (score > 139)
                {
                    ScoresOf140++;
                }
                else if (score > 99)
                {
                    ScoresOf100++;
                }
                else if (score > 59)
                {
                    ScoresOf60++;
                }

                if (group.Result == ThrowResult.CHECKEDOUT)
                {
                    AddToHighList(HighFinishes, score);
                }
            }
        }

        public void UnregisterScore(ThrowGroup group)
        {
            ThrownDarts -= group.DartCount;

            if (group.Result != ThrowResult.BUSTED)
            {
                byte score = group.PointSum;
                ThrownPoints -= score;

                HighScores.Remove(score);
                HighScores.Sort((a, b) => b.CompareTo(a));

                if (score == 180)
                {
                    ScoresOf180--;
                }
                else if (score > 139)
                {
                    ScoresOf140--;
                }
                else if (score > 99)
                {
                    ScoresOf100--;
                }
                else if (score > 59)
                {
                    ScoresOf60--;
                }

                if (group.Result == ThrowResult.CHECKEDOUT)
                {
                    HighFinishes.Remove(score);
                    HighFinishes.Sort((a, b) => b.CompareTo(a));
                }
            }
        }

        private void AddToHighList(List<byte> list, byte score)
        {
            list.Add(score);
            list.Sort((a, b) => b.CompareTo(a));
            try
            {
                list.RemoveRange(5, 1);
            }
            catch (Exception) { }
        }
    }
}