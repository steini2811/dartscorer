﻿using System;
using System.Reflection;

namespace DartScorer.Models
{
    class DartValueAttr: Attribute
    {
        public byte Value { get; }
        public bool IsDouble { get; }
        public bool IsTriple { get; }

        internal DartValueAttr(byte value, bool isDouble, bool isTriple) => (Value, IsDouble, IsTriple) = (value, isDouble, isTriple);
    }

    public static class DartValues
    {
        public static byte GetPoints(this DartValue dartValue)
        {
            DartValueAttr attr = GetAttr(dartValue);
            return (byte)(attr.IsTriple ? 3 * attr.Value : (attr.IsDouble ? 2 * attr.Value : attr.Value));
        }

        public static DartValue Parse(string valueString)
        {
            if (!Enum.TryParse<DartValue>(valueString, true, out DartValue dartValue))
            {
                dartValue = DartValue.NOT_THROWN;
            }

            return dartValue;
        }

        private static DartValueAttr GetAttr(DartValue dartValue)
        {
            return (DartValueAttr)Attribute.GetCustomAttribute(ForValue(dartValue), typeof(DartValueAttr));
        }

        private static MemberInfo ForValue(DartValue dartValue)
        {
            return typeof(DartValue).GetField(Enum.GetName(typeof(DartValue), dartValue));
        }
    }

    public enum DartValue
    {
        [DartValueAttr(20, false, true)] T20,
        [DartValueAttr(19, false, true)] T19,
        [DartValueAttr(18, false, true)] T18,
        [DartValueAttr(17, false, true)] T17,
        [DartValueAttr(16, false, true)] T16,
        [DartValueAttr(15, false, true)] T15,
        [DartValueAttr(14, false, true)] T14,
        [DartValueAttr(13, false, true)] T13,
        [DartValueAttr(12, false, true)] T12,
        [DartValueAttr(11, false, true)] T11,
        [DartValueAttr(10, false, true)] T10,
        [DartValueAttr(9, false, true)] T9,
        [DartValueAttr(8, false, true)] T8,
        [DartValueAttr(7, false, true)] T7,
        [DartValueAttr(6, false, true)] T6,
        [DartValueAttr(5, false, true)] T5,
        [DartValueAttr(4, false, true)] T4,
        [DartValueAttr(3, false, true)] T3,
        [DartValueAttr(2, false, true)] T2,
        [DartValueAttr(1, false, true)] T1,
        [DartValueAttr(25, true, false)] BULL,
        [DartValueAttr(20, true, false)] D20,
        [DartValueAttr(19, true, false)] D19,
        [DartValueAttr(18, true, false)] D18,
        [DartValueAttr(17, true, false)] D17,
        [DartValueAttr(16, true, false)] D16,
        [DartValueAttr(15, true, false)] D15,
        [DartValueAttr(14, true, false)] D14,
        [DartValueAttr(13, true, false)] D13,
        [DartValueAttr(12, true, false)] D12,
        [DartValueAttr(11, true, false)] D11,
        [DartValueAttr(10, true, false)] D10,
        [DartValueAttr(9, true, false)] D9,
        [DartValueAttr(8, true, false)] D8,
        [DartValueAttr(7, true, false)] D7,
        [DartValueAttr(6, true, false)] D6,
        [DartValueAttr(5, true, false)] D5,
        [DartValueAttr(4, true, false)] D4,
        [DartValueAttr(3, true, false)] D3,
        [DartValueAttr(2, true, false)] D2,
        [DartValueAttr(1, true, false)] D1,
        [DartValueAttr(25, false, false)] SBULL,
        [DartValueAttr(20, false, false)] S20,
        [DartValueAttr(19, false, false)] S19,
        [DartValueAttr(18, false, false)] S18,
        [DartValueAttr(17, false, false)] S17,
        [DartValueAttr(16, false, false)] S16,
        [DartValueAttr(15, false, false)] S15,
        [DartValueAttr(14, false, false)] S14,
        [DartValueAttr(13, false, false)] S13,
        [DartValueAttr(12, false, false)] S12,
        [DartValueAttr(11, false, false)] S11,
        [DartValueAttr(10, false, false)] S10,
        [DartValueAttr(9, false, false)] S9,
        [DartValueAttr(8, false, false)] S8,
        [DartValueAttr(7, false, false)] S7,
        [DartValueAttr(6, false, false)] S6,
        [DartValueAttr(5, false, false)] S5,
        [DartValueAttr(4, false, false)] S4,
        [DartValueAttr(3, false, false)] S3,
        [DartValueAttr(2, false, false)] S2,
        [DartValueAttr(1, false, false)] S1,
        [DartValueAttr(0, false, false)] NOT_HIT,
        [DartValueAttr(0, false, false)] NOT_THROWN
    }
}