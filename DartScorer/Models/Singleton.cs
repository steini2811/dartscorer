﻿using System;

namespace DartScorer.Models
{
    abstract public class Singleton<T> where T : Singleton<T>
    {
        private static T _instance = null;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (T) Activator.CreateInstance(typeof(T), true);
                }

                return _instance;
            }
        }
    }
}
