﻿namespace DartScorer.Models
{
    public enum CheckoutRule : byte
    {
        ALL_OUT, DOUBLE_OUT, TRIPLE_OUT, MASTER_OUT
    }
}