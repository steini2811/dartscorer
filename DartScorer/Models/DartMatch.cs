﻿using DartScorer.Models.Storage;
using System;

namespace DartScorer.Models
{
    public class DartMatch : IUniqueObject
    {
        private const byte MAXIMUM_UNDO_STEPS = 5;

        public int ID { get; set; }
        public PlayerInformation Player1 { get; set; }
        public PlayerInformation Player2 { get; set; }
        public MatchSetting Settings { get; set; }
        public bool Player1Active { get; set; }
        private readonly DropoutStack<MatchState> _undoStack = new DropoutStack<MatchState>(MAXIMUM_UNDO_STEPS);
        private readonly DropoutStack<MatchState> _redoStack = new DropoutStack<MatchState>(MAXIMUM_UNDO_STEPS);

        public int NumberOfSetsPlayed => Player1.Score.CurrentSets + Player2.Score.CurrentSets;

        public int NumberOfLegsPlayed => Player1.Score.CurrentLegs + Player2.Score.CurrentLegs;

        public bool MatchFinished => Math.Max(Player1.Score.CurrentSets, Player2.Score.CurrentSets) > (Settings.BestOfSets / 2)
                    || NumberOfSetsPlayed == Settings.BestOfSets;

        public bool UndoPossible => !_undoStack.IsEmpty;

        public bool RedoPossible => !_redoStack.IsEmpty;

        public DartMatch() { }

        public DartMatch(Player player1, Player player2, MatchSetting settings)
        {
            Player1 = new PlayerInformation(player1, settings.StartingPoints);
            Player2 = new PlayerInformation(player2, settings.StartingPoints);
            Settings = settings;
            Player1Active = settings.Player1Starts;

            ID = MatchStorage.Instance.AddItem(this);
        }

        public void Score(ThrowGroup throwGroup)
        {
            if (!MatchFinished)
            {
                PlayerInformation player = Player1Active ? Player1 : Player2;
                MatchState state = GetCurrentMatchState(throwGroup);

                player.Score.ReducePoints(throwGroup);
                switch (throwGroup.Result)
                {
                    case ThrowResult.OKAY:
                        ChangeActivePlayer();
                        break;
                    case ThrowResult.BUSTED:
                        ChangeActivePlayer();
                        break;
                    case ThrowResult.CHECKEDOUT:
                        StartNewLeg();
                        break;
                }

                player.Statistic.RegisterScore(throwGroup);
                player.Player.OverallStatistic.RegisterScore(throwGroup);

                _undoStack.Push(state);
                _redoStack.Clear();
            }
        }

        public void Undo()
        {
            MatchState lastState = _undoStack.Pop();
            ThrowGroup lastThrow = lastState.StepThrow;
            MatchState currentState = GetCurrentMatchState(lastThrow);

            PlayerInformation player = (!Player1Active ? Player1 : Player2);
            player.Statistic.UnregisterScore(lastThrow);
            player.Player.OverallStatistic.UnregisterScore(lastThrow);

            _redoStack.Push(currentState);
            RestoreState(lastState);
        }

        public void Redo()
        {
            MatchState nextState = _redoStack.Pop();
            ThrowGroup nextThrow = nextState.StepThrow;
            MatchState currentState = GetCurrentMatchState(nextThrow);

            PlayerInformation player = (Player1Active ? Player1 : Player2);
            player.Statistic.RegisterScore(nextThrow);
            player.Player.OverallStatistic.RegisterScore(nextThrow);

            _undoStack.Push(currentState);
            RestoreState(nextState);
        }

        private void ChangeActivePlayer()
        {
            Player1Active = !Player1Active;
        }

        private void StartNewLeg()
        {
            PlayerInformation winningPlayer = Player1Active ? Player1 : Player2;
            PlayerInformation otherPlayer = !Player1Active ? Player1 : Player2;

            winningPlayer.Score.CurrentLegs++;
            otherPlayer.Score.CurrentPoints = Settings.StartingPoints;
            winningPlayer.Score.CurrentPoints = Settings.StartingPoints;

            if (winningPlayer.Score.CurrentLegs > (Settings.BestOfLegs / 2))
            {
                winningPlayer.Score.CurrentSets++;
                otherPlayer.Score.CurrentLegs = 0;
                winningPlayer.Score.CurrentLegs = 0;
            }

            Player1Active = (NumberOfLegsPlayed + NumberOfSetsPlayed) % 2 == 0 ? Settings.Player1Starts : !Settings.Player1Starts;
        }

        private MatchState GetCurrentMatchState(ThrowGroup group)
        {
            return new MatchState(group, Player1Active, Player1.Score.Copy(), Player2.Score.Copy());
        }

        private void RestoreState(MatchState state)
        {
            Player1Active = state.Player1Active;
            Player1.Score = state.Player1Score;
            Player2.Score = state.Player2Score;
        }

        public string DisplayName
        {
            get
            {
                string currentStanding = Settings.BestOfSets > 1 ?
                    Player1.Score.CurrentSets + "-" + Player2.Score.CurrentSets :
                    Player1.Score.CurrentLegs + "-" + Player2.Score.CurrentLegs;
                return Player1.Player.PlayerName + " vs. " + Player2.Player.PlayerName + ": " + currentStanding;
            }
        }
    }
}