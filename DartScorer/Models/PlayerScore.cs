﻿using System.Collections.Generic;

namespace DartScorer.Models
{
    public class PlayerScore
    {
        public int CurrentPoints { get; set; }
        public int CurrentSets { get; set; }
        public int CurrentLegs { get; set; }
        public List<byte> CurrentLegScores { get; set; }

        public PlayerScore() { }
        public PlayerScore(int points)
            => (CurrentPoints, CurrentSets, CurrentLegs, CurrentLegScores) = (points, 0, 0, new List<byte>());
        private PlayerScore(int points, int sets, int legs)
            => (CurrentPoints, CurrentSets, CurrentLegs, CurrentLegScores) = (points, sets, legs, new List<byte>());

        public PlayerScore Copy()
        {
            PlayerScore score = new PlayerScore(CurrentPoints, CurrentSets, CurrentLegs);

            foreach (byte legScore in CurrentLegScores)
            {
                score.CurrentLegScores.Add(legScore);
            }

            return score;
        }

        public void ReducePoints(ThrowGroup group)
        {
            byte points = group.PointSum;
            CurrentLegScores.Add(points);

            if (CurrentPoints > points)
            {
                CurrentPoints = CurrentPoints - points;
                group.SetResult(ThrowResult.OKAY);
            }
            else if (CurrentPoints == points)
            {
                // check CheckoutRule
                group.SetResult(ThrowResult.CHECKEDOUT);
            }
            else
            {
                group.SetResult(ThrowResult.BUSTED);
            }
        }
    }
}