﻿using DartScorer.Models.Storage;

namespace DartScorer.Models
{
    public class Player : IUniqueObject
    {
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public PlayerStatistic OverallStatistic { get; set; }

        public Player() { }

        public Player(string name)
        {
            PlayerName = name;
            OverallStatistic = new PlayerStatistic();

            ID = PlayerStorage.Instance.AddItem(this);
        }
    }
}