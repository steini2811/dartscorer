﻿namespace DartScorer.Models
{
    public class ThrowGroup
    {
        public DartValue Dart1 { get; }
        public DartValue Dart2 { get; }
        public DartValue Dart3 { get; }
        public ThrowResult Result { get; private set; }

        public ThrowGroup(DartValue dart1) => (Dart1, Dart2, Dart3) = (dart1, DartValue.NOT_THROWN, DartValue.NOT_THROWN);
        public ThrowGroup(DartValue dart1, DartValue dart2) => (Dart1, Dart2, Dart3) = (dart1, dart2, DartValue.NOT_THROWN);
        public ThrowGroup(DartValue dart1, DartValue dart2, DartValue dart3) => (Dart1, Dart2, Dart3) = (dart1, dart2, dart3);

        public byte PointSum
        {
            get => (byte)(Dart1.GetPoints() + Dart2.GetPoints() + Dart3.GetPoints());
        }

        public byte DartCount
        {
            get
            {
                if (Dart2 == DartValue.NOT_THROWN)
                {
                    return 1;
                }
                if (Dart3 == DartValue.NOT_THROWN)
                {
                    return 2;
                }
                return 3;
            }
        }

        public DartValue LastDart
        {
            get
            {
                if (Dart3 != DartValue.NOT_THROWN)
                {
                    return Dart3;
                }
                if (Dart2 != DartValue.NOT_THROWN)
                {
                    return Dart2;
                }
                return Dart1;
            }
        }

        public void SetResult(ThrowResult result)
        {
            Result = result;
        }
    }
}