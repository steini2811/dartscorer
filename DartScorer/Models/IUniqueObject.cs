﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DartScorer.Models
{
    public interface IUniqueObject
    {
        int ID { get; set; }
    }
}
