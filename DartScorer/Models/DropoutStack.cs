﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DartScorer.Models
{
    public class DropoutStack<T>
    {
        /// <summary>
        /// Array containing the items.
        /// </summary>
        private readonly T[] _items;

        /// <summary>
        /// Position in array that a new item would take.
        /// </summary>
        private int _top;

        public int Count { get; private set; }
        public bool IsEmpty
        {
            get => Count == 0;
        }

        public DropoutStack(byte capacity)
        {
            _items = new T[capacity];
            _top = 0;
        }

        public void Push(T item)
        {
            Count = Count == _items.Length ? _items.Length : Count + 1;
            _items[_top] = item;
            _top = (_top + 1) % _items.Length;
        }

        public T Pop()
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException("This DropoutStack is empty.");
            }

            Count = Count - 1;
            _top = (_items.Length + _top - 1) % _items.Length;

            return _items[_top];
        }

        public T Peek()
        {
            if (IsEmpty)
            {
                throw new InvalidOperationException("This DropoutStack is empty.");
            }

            return _items[(_items.Length + _top - 1) % _items.Length];
        }

        public void Clear()
        {
            Count = 0;
            _top = 0;
        }
    }
}