﻿using System.Xml.Serialization;

namespace DartScorer.Models
{
    public class PlayerInformation
    {
        [XmlIgnore]
        public Player Player { get; set; }

        public int PlayerID { get; set; }
        public PlayerScore Score { get; set; }
        public PlayerStatistic Statistic { get; set; }

        public PlayerInformation() { }

        public PlayerInformation(Player player, int points)
        {
            Player = player;
            Score = new PlayerScore(points);
            Statistic = new PlayerStatistic();
            PlayerID = player.ID;
        }
    }
}