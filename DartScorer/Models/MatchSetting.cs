﻿namespace DartScorer.Models
{
    public class MatchSetting
    {
        public int StartingPoints { get; set; }
        public int BestOfSets { get; set; }
        public int BestOfLegs { get; set; }
        public bool Player1Starts { get; set; }
        public CheckoutRule Checkout { get; set; }

        public MatchSetting() { }

        public MatchSetting(int points, int sets, int legs, bool player1starts)
        {
            StartingPoints = points;
            BestOfSets = sets;
            BestOfLegs = legs;
            Player1Starts = player1starts;
            Checkout = CheckoutRule.ALL_OUT;
        }
    }
}