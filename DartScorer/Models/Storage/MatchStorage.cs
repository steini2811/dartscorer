﻿using System;
using System.Collections.Generic;

namespace DartScorer.Models.Storage
{
    public class MatchStorage : Singleton<MatchStorage>, IStorage<DartMatch>
    {
        public Dictionary<int, DartMatch> StorageItems { get; } = new Dictionary<int, DartMatch>();

        public string Path { get; } = AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + "\\Matches.xml";
        public object LockObject { get; } = new object();

        private MatchStorage() { }

        public void AssignPlayersToMatches()
        {
            foreach (DartMatch match in StorageItems.Values)
            {
                match.Player1.Player = PlayerStorage.Instance.StorageItems[match.Player1.PlayerID];
                match.Player2.Player = PlayerStorage.Instance.StorageItems[match.Player2.PlayerID];
            }
        }
    }
}