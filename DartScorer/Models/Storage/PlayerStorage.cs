﻿using System;
using System.Collections.Generic;

namespace DartScorer.Models.Storage
{
    public class PlayerStorage : Singleton<PlayerStorage>, IStorage<Player>
    {
        public Dictionary<int, Player> StorageItems { get; } = new Dictionary<int, Player>();

        public string Path { get; } = AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + "\\Players.xml";
        public object LockObject { get; } = new object();

        private PlayerStorage() { }
    }
}