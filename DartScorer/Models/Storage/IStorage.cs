﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace DartScorer.Models.Storage
{
    public interface IStorage<T> where T : IUniqueObject
    {
        Dictionary<int, T> StorageItems { get; }
        string Path { get; }
        object LockObject { get; }
    }

    public static class IStorageHelper
    {
        public static void ExportStorageItems<T>(this IStorage<T> storage) where T : IUniqueObject
        {
            List<T> items = new List<T>();
            foreach (T item in storage.StorageItems.Values)
            {
                items.Add(item);
            }

            try
            {
                using (StreamWriter writer = new StreamWriter(storage.Path))
                {
                    XmlSerializer serializer = new XmlSerializer(items.GetType());
                    serializer.Serialize(writer, items);
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException ||
                    ex is PathTooLongException || ex is DirectoryNotFoundException)
            {
            }
            catch (Exception)
            {
            }
        }

        public static void ImportStorageItems<T>(this IStorage<T> storage) where T : IUniqueObject
        {
            List<T> items = new List<T>();
            try
            {
                using (StreamReader reader = File.OpenText(storage.Path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
                    items = (List<T>)serializer.Deserialize(reader);
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            catch (FileNotFoundException)
            {
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException ||
                    ex is PathTooLongException || ex is DirectoryNotFoundException || ex is NotSupportedException)
            {
            }
            catch (Exception)
            {
            }

            foreach (T item in items)
            {
                try
                {
                    storage.StorageItems.Add(item.ID, item);
                }
                catch (ArgumentNullException) { }
                catch (ArgumentException) { }
            }
        }

        public static int AddItem<T>(this IStorage<T> storage, T item) where T : IUniqueObject
        {
            int itemID;
            lock (storage.LockObject)
            {
                itemID = storage.StorageItems.Count;
                storage.StorageItems.Add(itemID, item);
            }

            return itemID;
        }
    }
}
