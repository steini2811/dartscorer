﻿namespace DartScorer.Models
{
    public class MatchState
    {
        public ThrowGroup StepThrow { get; }
        public bool Player1Active { get; }
        public PlayerScore Player1Score { get; }
        public PlayerScore Player2Score { get; }

        public MatchState(ThrowGroup stepThrow, bool player1Active, PlayerScore player1, PlayerScore player2) =>
                (StepThrow, Player1Active, Player1Score, Player2Score) = (stepThrow, player1Active, player1, player2);
    }
}