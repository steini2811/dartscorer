﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DartScorer.Models
{
    public enum ThrowResult : byte
    {
        BUSTED, OKAY, CHECKEDOUT
    }
}