﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DartScorer
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "Home",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "NewMatch",
                url: "new",
                defaults: new { controller = "Home", action = "NewMatch" }
            );

            routes.MapRoute(
                name: "NewPlayer",
                url: "newplayer",
                defaults: new { controller = "Home", action = "NewPlayer" }
            );

            routes.MapRoute(
                name: "ShowScorer",
                url: "match/{id}",
                defaults: new { controller = "Home", action = "Scorer", id = -1 }
            );

            routes.MapRoute(
                name: "ShowScoreControl",
                url: "match/{id}/control",
                defaults: new { controller = "Home", action = "Control", id = -1 }
            );

            routes.MapRoute(
                name: "GetMatchData",
                url: "api/match/{id}",
                defaults: new { controller = "API", action = "Match", id = -1 }
            );

            routes.MapRoute(
                name: "SaveDataToXML",
                url: "api/saveall",
                defaults: new { controller = "API", action = "SaveToFile" }
            );

            routes.MapRoute(
                name: "Score",
                url: "match/{id}/score/{dart1}/{dart2}/{dart3}",
                defaults: new { controller = "API", action = "Score", id = -1, dart1 = "", dart2 = "", dart3 = "" }
            );

            routes.MapRoute(
                name: "Undo",
                url: "match/{id}/undo",
                defaults: new { controller = "API", action = "Undo", id = -1 }
            );

            routes.MapRoute(
                name: "Redo",
                url: "match/{id}/redo",
                defaults: new { controller = "API", action = "Redo", id = -1 }
            );

            /*
            routes.MapRoute(
                name: "Default_Fallback",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "API", action = "Match", id = UrlParameter.Optional }
            );
            */
        }
    }
}
